import configparser
import os

config = configparser.ConfigParser()
config.read('config.ini')

def auth_zero_domain():
    return config['Config']['AUTH0_DOMAIN']


def auth_zero_encrypt():
    list = []
    list.extend(config['Config']['ALGORITHMS'])
    return list


def client_id():
    return config['Config']['client_id']


def client_secret():
    return config['Config']['secrets']


def access_token():
    return config['Config']['ACCESS_TOKEN']


def auth_url():
    return config['Config']['AUTHORIZE_URL']


def callback_url():
    return config['Config']['CALLBACK_URL']