import Protobuf.mesousa_pb2
import base64


def generateportraitimage(image, scale, x, y, image_pb):
    image_pb.ImagePath = image
    image_pb.scale = scale
    image_pb.shiftX = x;
    image_pb.shiftY = y;


def generatestats(statlist, stat_pb):
    stat_pb.Pow = statlist[0]
    stat_pb.Ath = statlist[1]
    stat_pb.Brn = statlist[2]
    stat_pb.Con = statlist[3]
    stat_pb.Cha = statlist[4]
    stat_pb.Aff = statlist[5]


def generateskills(statlist, authority, stat_pb):
    stat_pb.Acting = statlist[0]
    stat_pb.Archery = statlist[18]
    stat_pb.Authority = statlist[9]
    stat_pb.BFG = statlist[19]
    stat_pb.Blades = statlist[20]
    stat_pb.Blunt = statlist[21]
    stat_pb.Body = statlist[29]
    stat_pb.Computer_Use = statlist[1]
    stat_pb.Deception = statlist[2]
    stat_pb.Disguise = statlist[11]
    stat_pb.Dodge = statlist[22]
    stat_pb.Energy = statlist[30]
    stat_pb.Hand_to_Hand = statlist[23]
    stat_pb.Hide_Stuff = statlist[10]
    stat_pb.Influence = statlist[3]
    stat_pb.Jump = statlist[12]
    stat_pb.Magic_Chi = statlist[4]
    stat_pb.Marksmanship = statlist[24]
    stat_pb.Mecha_Weapons = statlist[25]
    stat_pb.Mechanics = statlist[13]
    stat_pb.Medicine = statlist[21]
    stat_pb.Pilot_Mecha = statlist[14]
    stat_pb.Pilot_Vehicle = statlist[6]
    stat_pb.Resilience = statlist[28]
    stat_pb.Run = statlist[15]
    stat_pb.Science = statlist[7]
    stat_pb.Search = statlist[16]
    stat_pb.Special_Weapon = statlist[26]
    stat_pb.Stealth = statlist[17]
    stat_pb.Yeet = statlist[27]
    stat_pb.Yoink = statlist[8]
    stat_pb.AuthorityType = authority


def generate_item(name, type, desc, integrity, hs, cost, pb_item):
    """

    :type pb_item: Protobuf.mesousa_pb2.SheetItem
    """
    pb_item.ItemName = name
    pb_item.ItemType = type
    pb_item.Description = desc
    pb_item.Integrity = integrity
    pb_item.HideStuff = hs
    pb_item.Cost = cost


def generate_skill(name, description, cost=None, value=None):
    skill: Protobuf.mesousa_pb2.SpecialSkill = Protobuf.mesousa_pb2.SpecialSkill()
    skill.SkillName = name
    skill.Description = description
    if cost:
        skill.SkillCost = cost
    if value:
        skill.SkillValue = value
    return skill


def GenerateSheet1():
    sheet1: Protobuf.mesousa_pb2.CharacterSheet = Protobuf.mesousa_pb2.CharacterSheet()
    sheet1.Name = "Mesousa"
    sheet1.Series = 'Nexus'
    sheet1.Type = Protobuf.mesousa_pb2.CharacterSheet.Anime
    sheet1.Background = "Mesousa is a very sad bunny"
    generatestats([1, 1, 3, 1, 3, 1], sheet1.Stats)
    generateskills([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5], '',
                   sheet1.CharacterSkills)
    generate_item('Bandanna', 'Clothing', 'A small red bandanna', 5, 3, 2, sheet1.MainItem)
    sheet1.StartingItems.append('Soda')
    mesousa_powers = []
    mesousa_powers.append(generate_skill('But I Don’t Have Any Thumbs!',
                                         'Anytime in a scene where Mesousa attempts to use an item that requires mesousa to hold or grasp, test Athletics against this skill. If successful, Mesousa is able to continue using the item with little trouble for the rest of the scene. If Mesousa fails, he must immediately drop the item. He must then test athletics against this skill in order to pick the item back up. Once successful, he can use the item without dropping it for the rest of the scene. Any dropped item may be picked up by anyone else without testing.',
                                         value=2))
    mesousa_powers.append(generate_skill('M-Maybe I do Have a Hidden Power...',
                                         'Other players may try to encourage Mesousa to complete a task. Test against Mesousa’s Confidence, using a skill equal to the number of people actively encouraging Mesousa to complete the task. If successful, Mesousa gains +2 to his confidence for the rest of the scene/battle and -1 to his next Stat or skill check to actually accomplish the task'))
    mesousa_powers.append(generate_skill('Convenient Replacement',
                                         'When another player attempts to disguise Mesousa as another player in game, they may add the value of this skill  to thier disguise skill.',
                                         value=2))
    mesousa_powers.append(generate_skill('Langomorphic Versilility',
                                         'Mesousa may be used as BFG ammo. If he hits the target, he takes the same amount of damage that the weapon he was fires out of deals. Otherwise, he is just launched out of the scene.'))
    mesousa_powers.append(generate_skill('Death-prone Mascot',
                                         'As Mesousa exists essentially for bad things to happen to him, taking horrific amounts of damage never really seems to kill him. Mesousa can return to the game after 15 minutes.'))
    sheet1.Powers.extend(mesousa_powers)
    generateportraitimage("\\static\\Images\\portrait_placeholder.png", 1,0,0, sheet1.Portrait)
    generateportraitimage("\\static\\Images\\item-placeholder.png", 1,0,0, sheet1.MainItem.ItemImage)
    generateportraitimage("\\static\\Images\\character_image_placeholder.png", 1,0,0, sheet1.FullImage)
    return sheet1


def GenerateSheet2():
    sheet1: Protobuf.mesousa_pb2.CharacterSheet = Protobuf.mesousa_pb2.CharacterSheet()
    sheet1.Name = "Rabbit Witch"
    sheet1.Series = 'Witch no Usa'
    sheet1.Type = Protobuf.mesousa_pb2.CharacterSheet.Anime
    sheet1.Background = 'Her goal, ostensibly, is to help her mentor bring about Walpurgis Night but more realistically it’s trying not to get beat up too much to not be able to Karaoke.'
    generatestats([3, 4, 2, 5, 3, 1], sheet1.Stats)
    generateskills([1, 1, 3, 2, 4, 1, 0, 0, 2, 0, 2, 1, 4, 0, 4, 3, 3, 4, 0, 0, 0, 1, 2, 2, 0, 3, 0, 1, 0, 8, 24],
                   'Witches', sheet1.CharacterSkills)
    generate_item('bigbun', 'Companion',
                  'Run 2, Jump 4, H2H 3, Power 3, Athletics 2, Brains 2, Confidence 2, Dodge 2, Stealth 3 bigbun is Kasumi’s familiar. she can summon bigbun as an action for 1 energy, and dismiss him as an action for 0 energy. bigbun can assist Tanpop in battles or attack independently, as long as she is in the scene.  If killed, he is out of game for the same amount of time as a character.  He can also carry things for her. If she is knocked out he will attempt to flee the scene with her and take her to a hospital <br> Energy:10',
                  12, -5, 7, sheet1.MainItem)
    sheet1.StartingItems.extend(['Backpack', 'Broomstick', 'Fear Illusion'])
    mesousa_powers = []
    mesousa_powers.append(generate_skill('White Rabbit Paper Knights!',
                                         'she’s main strategy is to summon an army of rabbits from paper cutouts she keeps in her robes. <br>Plan C Cost 2 Energy.  A pair of rabbits grab she and high-tail it out of the scene, allowing her to escape by paying the energy cost. she may add her Magic Score to her Dodge, Run and other skills used to escape a scene or pursuers for one full round.  You must be attempting to escape to use this.  <br>Plan B Cost 3 Energy.  she may summon in a wave of Rabbits to attack a group of enemies by testing against a difficulty of her choosing.  They deal one damage to all opponents in an area every round until they are defeated, the opponents surrender, or she calls them off.  The Hoard has 0 Dodge, no Resilience and Body equal to double the difficulty against which you test.   The rabbits can be attacked directly or through area affect attacks. They may also, instead of damaging all character in a scene, assist either she or bigbun in an action, as normal (granting a +1 Assistance bonus). <br>Plan A Cost: Variable she can summon her rabbits to perform menial and time consuming tasks such as carrying things or following simple building instructions. They are not capeble of fighting or doing anything that requires a high level of skill or knowledge (such as crack a safe or assasinating someone). Their skill in this task is equal to the number of rabbits she summons for the task, equal to 3 energy per rabbit used (minus any modifiers at the GM’s discretion).',
                                         value=2))
    mesousa_powers.append(generate_skill('Mecha Bun',
                                         'At any point she can spend 5 energy and test her Magic/Chi against a difficulty of this skill (As if casting a spell) to turn bigbun into a Grade 2 Mecha. As a Mecha, bigbun has the following stats: <br>Power: 8		Athletics: 2 	Run: 2		Jump:2 <br>Resilience: 10	Grade: 2		Structure: 11	Energy: 10 <br> she piliots bigbun by standing on his head. When mecha bigbun is destroyed, he returns to normal bigbun at the end of the scene.',
                                         value=1, cost=5))
    mesousa_powers.append(generate_skill('Witch',
                                         'she is a witch, commited to causing chaos and eventually WalpurgisNight. How commited to that she may be is another matter.  She cannot be commanded to do anything by the Authority Skill applying to Witches, Wizards, or supernatural beings, though other methods (magic, intimidation) still work.',
                                         value=2))
    mesousa_powers.append(generate_skill('WHY IS THIS HAPPENING',
                                         'she has a terrible habit of being in the wrong place at the wrong time. If in a scene where something happens to a randomly selected character, it will always affect she. If the effect would cause more than 5 damage to she, she is instead ejected fromt the scene, Team Rocket style.'))
    sheet1.Powers.extend(mesousa_powers)
    generateportraitimage("\\static\\Images\\portrait_placeholder.png", 1,0,0, sheet1.Portrait)
    generateportraitimage("\\static\\Images\\item-placeholder.png", 1,0,0, sheet1.MainItem.ItemImage)
    generateportraitimage("\\static\\Images\\character_image_placeholder.png", 1,0,0, sheet1.FullImage)
    return sheet1


def GetFakeSheets():
    sheet1 = GenerateSheet1()
    sheet2 = GenerateSheet2()
    return [sheet1, sheet2]
