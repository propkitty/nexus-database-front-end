from security import sanitize_formatted_input

class NexusSheet(object):

    def __init__(self, form_data):
        self.form_data = form_data
        self.name = form_data['char-name']
        self.series = SeriesData(form_data)
        self.background = sanitize_formatted_input(form_data['char-background'])
        self.pic_small = form_data['char-pic-small']
        self.pic_large = form_data['char-pic-large']
        self.stats = StatsBlock(form_data)
        self.skills = SkillBlock(form_data)
        self.powers = self.make_power_list()
        self.sheet_item = self.make_sheet_item()
        self.startingItems = sanitize_formatted_input(form_data['char-startingItems'])

    def item_info(self):
        return self.sheet_item.item_info()

    def make_power_list(self):
        final_list = []
        index = 1
        while True:
            if self.form_data.get('char-power' + str(index) + 'Name', None) is not None:
                final_list.append(SpecialAbility(self.form_data, index))
                index += 1
            else:
                break

        return final_list

    def make_sheet_item(self):
        if self.form_data.get('char-item-item-name', None) is not None:
            return SheetItem(self.form_data)
        elif self.form_data.get('char-item-mecha-name', None) is not None:
            return SheetItemMecha(self.form_data)
        elif self.form_data.get('char-item-companion-name', None) is not None:
            return SheetItemCompanion(self.form_data)
        else:
            return SheetItemPokemon(self.form_data)


class SeriesData(object):

    def __init__(self, form_data):
        self.name = form_data['series-name']
       # self.medium = form_data['series-medium']
        self.id = self.get_series_id_from_db()

    def get_series_id_from_db(self):
        return "1"


class StatsBlock:

    def __init__(self, form_data):
        self.power = form_data['char-power']
        self.athletics = form_data['char-athletics']
        self.brains = form_data['char-brains']
        self.confidence = form_data['char-confidence']
        self.charisma = form_data['char-charisma']
        self.affluence = form_data['char-affluence']
        self.body = form_data['char-body']
        self.energy = form_data['char-energy']
        self.resilience = form_data['char-resilience']


class SkillBlock:

    def __init__(self, form_data):
        self.acting = form_data['char-acting']
        self.archery = form_data['char-archery']
        self.authority = form_data['char-authority']
        self.bfg = form_data['char-bfg']
        self.blades = form_data['char-blades']
        self.blunt = form_data['char-blunt']
        self.computerUse = form_data['char-computerUse']
        self.deception = form_data['char-deception']
        self.disguise = form_data['char-disguise']
        self.dodge = form_data['char-dodge']
        self.h2h = form_data['char-h2h']
        self.hide_Stuff = form_data['char-hideStuff']
        self.influence = form_data['char-influence']
        self.jump = form_data['char-jump']
        self.magicChi = form_data['char-magicChi']
        self.marksmanship = form_data['char-marksmanship']
        self.mechaWeapons = form_data['char-mechaWeapons']
        self.mechanics = form_data['char-mechanics']
        self.medicine = form_data['char-medicine']
        self.pilotMecha = form_data['char-pilotMecha']
        self.pilotVehicle = form_data['char-pilotVehicle']
        self.resilience = form_data['char-resilience']
        self.run = form_data['char-Run']
        self.science = form_data['char-science']
        self.search = form_data['char-search']
        self.specialWeapons = form_data['char-specialWeapons']
        self.stealth = form_data['char-stealth']
        self.throw = form_data['char-throw']
        self.yoink = form_data['char-yoink']
        self.authorityOver = form_data['char-authorityOver']


class SheetItem:
    type = 'Base'

    def __init__(self, form_data):
        self.name = form_data['char-item-item-name']
        self.type = form_data['char-item-item-type']
        self.picture = form_data['char-item-item-picture']
        self.description = sanitize_formatted_input(form_data['char-item-item-description'])
        self.integrity = form_data['char-item-item-integrity']
        self.HS = form_data['char-item-item-HS']
        self.cost = form_data['char-item-item-cost']
        self.uses = form_data['char-item-item-uses']

    def item_info(self):
        return {'Name': self.name, 'Type': self.type, 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = self.description + ' <br>Integrity: ' + self.integrity + '<br> HS: ' + self.HS + '<br>Cost: ' + self.cost
        if self.uses != '':
            final_desc + "<br> Uses: " + self.uses
        return final_desc


class SheetItemMecha:
    type = 'Mech'

    def __init__(self, form_data):
        self.name = form_data['char-item-mecha-name']
        self.picture = form_data['char-item-mecha-picture']
        self.integrity = form_data['char-item-mecha-integrity']
        self.energy = form_data['char-item-mecha-energy']
        self.resilience = form_data['char-item-mecha-resilience']
        self.grade = form_data['char-item-mecha-grade']
        self.power = form_data['char-item-mecha-power']
        self.athletics = form_data['char-item-mecha-athletics']
        self.run = form_data['char-item-mecha-run']
        self.fly = form_data['char-item-mecha-fly']
        self.jump = form_data['char-item-mecha-jump']
        self.h2h = form_data['char-item-mecha-h2h']
        self.dodge = form_data['char-item-mecha-dodge']
        self.abilities = sanitize_formatted_input(form_data['char-item-mecha-abilities'])

    def item_info(self):
        return {'Name': self.name, 'Type': 'Mecha', 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = 'Pow:' + self.power + ' Ath:' + self.athletics + ' Run:' + self.run + '  Fly: ' + self.fly + \
                     '<br>Jump:' + self.jump + ' H2H:' + self.h2h + ' Dodge:' + self.dodge + ' Integrity:' + \
                     self.integrity + '<br>Grade:' + self.grade + ' Energy:' + self.energy + '<br>' + self.abilities
        return final_desc


class SheetItemCompanion:
    type = 'Companion'

    def __init__(self, form_data):
        self.name = form_data['char-item-companion-name']
        self.picture = form_data['char-item-companion-picture']
        self.body = form_data['char-item-companion-body']
        self.energy = form_data['char-item-companion-energy']
        self.resilience = form_data['char-item-companion-resilience']
        self.grade = form_data['char-item-companion-grade']
        self.power = form_data['char-item-companion-power']
        self.athletics = form_data['char-item-companion-athletics']
        self.run = form_data['char-item-companion-run']
        self.fly = form_data['char-item-companion-fly']
        self.jump = form_data['char-item-companion-jump']
        self.h2h = form_data['char-item-companion-h2h']
        self.dodge = form_data['char-item-companion-dodge']
        self.abilities = sanitize_formatted_input(form_data['char-item-companion-abilities'])

    def item_info(self):
        return {'Name': self.name, 'Type': 'Companion', 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = 'Pow:' + self.power + ' Ath:' + self.athletics + ' Run:' + self.run + '  Fly: ' + self.fly + \
                     '<br>Jump:' + self.jump + ' H2H:' + self.h2h + ' Dodge:' + self.dodge + ' Body:' + \
                     self.body + '<br>'+self.grade()+' Energy:' + self.energy + '<br>' + self.abilities
        return final_desc

    def grade(self):
        if self.grade != '':
            return 'Grade:' + self.grade
        else:
            return ''


class SheetItemPokemon:
    type = 'Pokemon'

    def __init__(self, form_data):
        self.name = form_data['pokemon-name']
        self.picture = form_data['pokemon-picture']
        self.pkmntype = form_data['pokemon-type']
        self.evolutionStage = form_data['pokemon-evolutionStage']
        self.body = form_data['pokemon-body']
        self.energy = form_data['pokemon-energy']
        self.resilience = form_data['pokemon-resilience']
        self.grade = form_data['pokemon-grade']
        self.power = form_data['pokemon-power']
        self.athletics = form_data['pokemon-athletics']
        self.run = form_data['pokemon-run']
        self.fly = form_data['pokemon-fly']
        self.jump = form_data['pokemon-jump']
        self.h2h = form_data['pokemon-h2h']
        self.dodge = form_data['pokemon-dodge']
        self.moves = sanitize_formatted_input(form_data['pokemon-moves'])

    def item_info(self):
        return {'Name': self.name, 'Type': 'Pokemon', 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = 'Pow:' + self.power + ' Ath:' + self.athletics + ' Run:' + self.run + '  Fly: ' + self.fly + \
                     '<br>Jump:' + self.jump + ' H2H:' + self.h2h + ' Dodge:' + self.dodge + ' Body:' + \
                     self.body + '<br>'+self.grade()+' Energy:' + self.energy + '<br>' + self.moves
        return final_desc

    def grade(self):
        if self.grade != '':
            return 'Grade:' + self.grade
        else:
            return ''


class SpecialAbility:

    def __init__(self, form_data, index):
        self.powerName = form_data['char-power' + str(index) + 'Name']
        self.powerSkill = form_data['char-power' + str(index) + 'Skill']
        self.powerCost = form_data['char-power' + str(index) + 'Cost']
        self.powerDescription = sanitize_formatted_input(form_data['char-power' + str(index) + 'Description'])
        self.position = index
